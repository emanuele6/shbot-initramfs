.PHONY: clean requirements

PATH := build/bin:$(PATH)

shells = build/bin/bash+

bash1_version = 1.14.7
shells += build/bin/bash1

bash2_version = 2.05b.13
shells += build/bin/bash2

bash32_version = 3.2.57
shells += build/bin/bash32

bash42_version = 4.2.53
shells += build/bin/bash42
bash43_version = 4.3.46
shells += build/bin/bash43
bash44_version = 4.4.23
shells += build/bin/bash44

bash50_version = 5.0.18
shells += build/bin/bash50
bash51_version = 5.1.4
shells += build/bin/bash51

dash_version = 0.5.9
shells += build/bin/dash

awks = build/bin/oawk build/bin/nawk

locales += build/locales/de_DE.UTF-8
locales += build/locales/en_US.UTF-8
locales += build/locales/et_EE.UTF-8
locales += build/locales/nb_NO.UTF-8

initramfs.cpio.gz: requirements initramfs
	{ cd initramfs && pax -x sv4cpio -w .; } | gzip -9 > initramfs.cpio.gz

requirements:
	@bash -c '\
	# These commands must be available \
	type aclocal autoconf bison docker flex gcc git gzip make pax \
	'

clean:
	rm -rf build/*/ initramfs/
	rm -f initramfs.cpio.gz fifo *~

## mksh

sources += sources/mksh
shells += build/bin/mksh
manpages += build/man/man1/mksh.1

build/dash-%: sources/dash
	scripts/extract-from-git "$@"
	touch "$@"

sources/dash:
	git clone git://git.kernel.org/pub/scm/utils/dash/dash.git "$@"

sources/mksh:
	git clone https://github.com/MirBSD/mksh.git "$@"

build/mksh: sources/mksh
	rm -rf "$@"
	mkdir -p "$@"
	cd "$@" && sh "../../$</Build.sh"

build/bin/mksh: build/mksh
	mkdir -p "$(@D)"
	cp "$</mksh" "$@"
	scripts/add-trigger 'm#' "setsid mksh -l" "mksh $$("$@" -c 'echo "$${KSH_VERSION#* * }"')"

build/man/man1/mksh.1: sources/mksh
	mkdir -p "$(@D)"
	cp "$</mksh.1" "$@"

## bash

sources += sources/bash


sources/bash:
	git clone https://gitlab.com/geirha/bash.git "$@"

build/bash-%: sources/bash
	rm -rf "$@"
	scripts/extract-from-git "$@"

build/bin/dash: build/dash-$(dash_version)
	scripts/build-shell dash "$(dash_version)" dash
	scripts/add-trigger 'd#'  'setsid dash -l' "dash-$(dash_version)" \
	                    'sh#' 'setsid sh -l'   "d#"
build/bin/bash1: build/bash-$(bash1_version)
	scripts/build-shell bash $(bash1_version) bash1
	scripts/add-trigger '1#' "ln -sf bash1 /bin/bash;setsid bash -login" bash-1.14
build/bin/bash2: build/bash-$(bash2_version)
	scripts/build-shell bash $(bash2_version) bash2
	scripts/add-trigger '2#' "ln -sf bash2 /bin/bash;setsid bash -l" bash-2.05b
build/bin/bash32: build/bash-$(bash32_version)
	scripts/build-shell bash $(bash32_version) bash32
	scripts/add-trigger '32#' "ln -sf bash32 /bin/bash;setsid bash -l" bash-3.2 \
	                    '3#'  "ln -sf bash3 /bin/bash;setsid bash -l"  "32#"
build/bin/bash42: build/bash-$(bash42_version)
	scripts/build-shell bash $(bash42_version) bash42
	scripts/add-trigger '42#' "ln -sf bash42 /bin/bash;setsid bash -l" bash-4.2
build/bin/bash43: build/bash-$(bash43_version)
	scripts/build-shell bash $(bash43_version) bash43
	scripts/add-trigger '43#' "ln -sf bash43 /bin/bash;setsid bash -l" bash-4.3
build/bin/bash44: build/bash-$(bash44_version)
	scripts/build-shell bash $(bash44_version) bash44
	scripts/add-trigger '44#' "ln -sf bash44 /bin/bash;setsid bash -l" bash-4.4 \
	                    '4#'  "ln -sf bash4 /bin/bash;setsid bash -l"  "44#"
build/bin/bash50: build/bash-$(bash50_version)
	scripts/build-shell bash $(bash50_version) bash50
	scripts/add-trigger '50#' "ln -sf bash50 /bin/bash;setsid bash -l" bash-5.0
build/bin/bash51: build/bash-$(bash51_version)
	LOADABLES=1 scripts/build-shell bash $(bash51_version) bash51
	scripts/add-trigger '51#' "ln -sf bash51 /bin/bash;setsid bash -l" bash-5.1 \
	                    '5#'  "ln -sf bash5 /bin/bash;setsid bash -l"  "51#" \
	                    '#'  "LOADABLES=1 setsid bash -l"  "51#"
build/bash-latest: build/bash-$(bash51_version)
	ln -s bash-$(bash51_version) "$@"

build/bin/bash+: build/bash-devel
	scripts/build-shell bash devel bash+
	scripts/add-trigger '+#' "ln -sf bash+ /bin/bash; setsid bash -l" "bash-devel"

## gawk

sources/gawk:
	git clone git://git.savannah.gnu.org/gawk.git "$@"

sources += sources/gawk

build/gawk-%: sources/gawk
	scripts/extract-from-git "$@"
	cd "$@" && ./configure && make

build/bin/gawk5: build/gawk-5.1.0
	cp "$</gawk" "$@"
build/man/man1/gawk5.1: build/gawk-5.1.0
	cp "$</doc/gawk.1" "$@"
awks += build/bin/gawk5
manpages += build/man/man1/gawk5.1



sources/mawk:
	git clone https://github.com/ThomasDickey/mawk-snapshots.git "$@"
build/bin/mawk133:
	cp /usr/bin/mawk "$@"
build/man/man1/mawk133.1:
	gzip -cd </usr/share/man/man1/mawk.1.gz >"$@"
awks += build/bin/mawk133
manpages += build/man/man1/mawk133.1

build/mawk-1.3.4: sources/mawk
	scripts/extract-from-git "$@"
	cd "$@" && ./configure && make
build/bin/mawk134: build/mawk-1.3.4
	cp "$</mawk" "$@"
build/man/man1/mawk134.1: build/mawk-1.3.4
	cp "$</man/mawk.1" "$@"
awks += build/bin/mawk134
manpages += build/man/man1/mawk134.1

## heirloom-sh
sources += sources/heirloom-project
shells += build/bin/bsh build/bin/jsh
manpages += build/man/man1/bsh.1 build/man/man1/jsh.1

sources/heirloom-project/%:
	git clone https://github.com/eunuchs/heirloom-project.git sources/heirloom-project

build/heirloom-sh: sources/heirloom-project/heirloom/heirloom-sh
	rm -rf "$@"
	mkdir -p "$@"
	cd "$@" && ln -s "../../$<"/* ./
	make -C "$@"

build/bin/bsh: build/heirloom-sh
	cp "$</sh" "$@"
	scripts/add-trigger 'b#' 'sh -c ". /etc/profile";read -r;PS1= exec bsh -ic "\x24REPLY;echo o>/proc/sysrq-trigger"'      bourne

build/man/man1/bsh.1: build/heirloom-sh
	cp "$</sh.1" "$@"

build/bin/jsh: build/bin/bsh
	ln -s "$(<F)" "$@"
	scripts/add-trigger 'j#' 'sh -c ". /etc/profile";read -r;PS1= exec jsh -ic "\x24REPLY;echo o>/proc/sysrq-trigger"'     'bourne(w/job)'

build/man/man1/jsh.1: build/man/man1/bsh.1
	ln -s "$(<F)" "$@"

build/heirloom/%: build/heirloom
	mkdir -p "$@"
	$(MAKE) SHELL=/bin/sh -C "$(@D)" "$(@F)/Makefile"
	$(MAKE) SHELL=/bin/sh -C "$@"

build/heirloom: sources/heirloom-project/heirloom/heirloom
	rm -rf "$@"
	mkdir -p "$@"
	scripts/lndir "../../$<" "$@"

build/bin/oawk: build/heirloom/libcommon build/heirloom/libuxre build/heirloom/oawk build/heirloom/oawk/awk
	cp build/heirloom/oawk/awk "$@"

build/man/man1/oawk.1: build/heirloom/oawk/
	cp "$</awk.1" "$@"

sources/onetrueawk:
	git clone https://github.com/onetrueawk/awk "$@"

build/onetrueawk: sources/onetrueawk
	rm -rf "$@"
	mkdir -p "$@"
	{ cd "$<" && git archive --format=tar HEAD; } | { cd "$@" && pax -r; }
	$(MAKE) -C "$@"


build/bin/nawk: build/onetrueawk
	cp "$</a.out" "$@"

build/man/man1/nawk.1: build/onetrueawk
	cp "$</awk.1" "$@"

build/bin/adu:
	scripts/build-adu
build/bin/ex:
	scripts/build-ex-vi

build/locales:
	mkdir -p "$@"

build/locales/%: build/locales
	set -x; \
	x="$@" locale=$${x##*/} lang=$${locale%.*} enc=$${locale#"$$lang."}; \
	localedef --no-archive -c -i "$$lang" -f "$$enc" "$@"

sources/bash-builtins:
	git clone https://github.com/geirha/bash-builtins.git "$@"
build/bash-builtins: sources/bash-builtins build/bash-latest
	rm -rf "$@"
	mkdir -p "$@"
	{ cd "$<" && git archive --format=tar HEAD; } | { cd "$@" && pax -r; }
	$(MAKE) -C build/bash-latest  prefix="../bash-headers" install-headers
	$(MAKE) -C build/bash-latest/examples/loadables prefix="../../../bash-headers" everything install-dev
	$(MAKE) -C "$@" Makefile.inc
	set -x; for builtin in asort csv md5; do \
          if [ ! -x "build/loadables/$$builtin" ]; then \
            $(MAKE) -C "$@" -f Makefile.inc prefix="../bash-headers" "$$builtin"; \
          fi; \
        done
	for file in "$@"/*; do \
	    [ -x "$$file" ] && [ -f "$$file" ] || continue; \
	    cp -v "$$file" build/loadables/; \
	done

build/jq/jq build/jq/jq.1:
	scripts/build-jq

build/bin/jq build/man/man1/jq.1: build/jq/jq build/jq/jq.1
	cp build/jq/jq build/bin/jq
	cp build/jq/jq.1 build/man/man1/jq.1

## seq

sources += sources/seq
sources/seq:
	git clone https://github.com/phy1729/seq.git "$@"
build/seq: sources/seq
	rm -rf "$@"
	mkdir -p "$@"
	cd "$@" && ln -s ../../sources/seq/* . && make
build/bin/seq: build/seq
	cp "$</seq" "$@"


initramfs: $(shells) $(awks) $(manpages) build/bin/adu build/bin/ex scripts/generate-initramfs $(locales) build/bash-builtins build/bin/jq build/bin/seq
	scripts/generate-initramfs

